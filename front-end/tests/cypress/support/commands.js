/**
 * Create a new user and log them in.
 *
 */
Cypress.Commands.add('login', () => {
  // TODO implement your own logic here, e.g.:
  return cy
    .request({
      method: 'POST',
      headers: {
        'x-cypress-header': true,
      },
      url: Cypress.env('apiUrl') + '/users/login',
      body: { email: 'test@example.com', password: 'password' },
      log: false,
    })
    .then(({ body }) => {
      // Store the token in the local storage or a cookie

      // Reload the page so that the token is used
      cy.reload();

      Cypress.log({
        name: 'login',
        message: JSON.stringify(body),
      });
    });
});

/**
 * Create a new factory
 *
 * @param {String} model
 * @param {Number|null} times
 * @param {Object} attributes
 *
 * @example
 * cy.create({ factory: '\\App\\User' })
 * cy.create({
 *      factory: '\\App\\User',
 *      state: { email: 'test@test.com' }
 * })
 * cy.create({
 *      factory: '\\App\\User',
 *      with_associations: true // Will call the withAssociations method on the factory
 * })
 * cy.create({
 *      factory: '\\App\\User',
 *      state: { email: 'test@test.com' },
 *      with: [{ model: 'Roles', state: {level: 5}}]
 * })
 * cy.create({
 *      factory: '\\App\\User',
 *      state: { email: 'test@test.com' },
 *      custom_methods: [{ name: 'withRoles', parameters: [5]}]
 * })
 */
Cypress.Commands.add(
    'create',
    (params) => {
        return cy.request({
            method: 'POST',
            headers: {
              'x-cypress-header': true,
            },
            url: '/cakephp-cypress/create',
            body: params,
            log: false,
        })
            .then((response) => {
                Cypress.log({
                    name: 'create',
                    message: params.factory,
                    consoleProps: () => ({body: true}),
                });
            })
            .its('body', {log: false});
    }
);

/**
 * Refresh the database state.
 *
 * @param {Object} options
 *
 * @example cy.refreshDatabase();
 */
Cypress.Commands.add('refreshDatabase', (options = {}) => {
    cy.request({
        method: 'POST',
        headers: {
          'x-cypress-header': true,
        },
        url: '/cakephp-cypress/refresh-database',
        body: {
            fixtures: [
                "app.Users",
            ]
        },
        log: false,
    }).then(({status}) => {
        Cypress.log({
            name: 'refresh database',
            message: JSON.stringify(status),
            consoleProps: () => ({status}),
        });
    }).its('status', {log: false});
});
