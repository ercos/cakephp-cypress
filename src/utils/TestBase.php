<?php
declare(strict_types=1);

namespace Ercos\CakephpCypress\utils;

use Cake\TestSuite\TestCase;

class TestBase extends TestCase
{
    public $dropTables = true;

    public function __construct(array $fixtures)
    {
        parent::__construct();
        $this->fixtures = $fixtures;
    }
}
