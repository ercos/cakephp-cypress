<?php
declare(strict_types=1);

namespace Ercos\CakephpCypress\Controller;

use Cake\Controller\Controller;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\Fixture\FixtureManager;
use Ercos\CakephpCypress\utils\TestBase;
use Migrations\Migrations;

class CypressController extends Controller
{
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');

        $this->autoRender = false;
    }

    public function refreshDatabase()
    {
        $data = $this->getRequest()->getData();

        if (env('SQL_DUMP_FOLDER', false)) {
            $connection = ConnectionManager::get('default');

            $tables = $connection->getSchemaCollection()->listTables();
            $connection->execute('SET FOREIGN_KEY_CHECKS = 0');

            foreach ($tables as $table) {
                $connection->execute('drop table '.$table);
            }

            $connection->execute(file_get_contents(getcwd().'/..' . env('SQL_DUMP_FOLDER')));

            $migrations = new Migrations();
            $migrations->migrate();
        }

        if (isset($data['fixtures'])) {
            $fixturesManager = new FixtureManager();
            $testCase = new TestBase($data['fixtures']);
            $fixturesManager->fixturize($testCase);
            $fixturesManager->load($testCase);
        }

        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode(['data' => true]));
    }

    public function create()
    {
        $data = $this->getRequest()->getData();

        $factory = $data['factory']::make($data['state'] ?? null);

        if (!empty($data['with'])) {
            foreach ($data['with'] as $with) {
                $factory->with($with['model'], $with['state'] ?? null);
            }
        }

        if (!empty($data['custom_methods'])) {
            foreach ($data['custom_methods'] as $customMethod) {
                $methodName = $customMethod['name'];
                $factory = $factory->$methodName(...$customMethod['parameters']);
            }
        }

        if (!empty($data['with_associations'])) {
            $factory->withAssociations();
        }

        $model = $factory->persist();

        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode(['data' => $model]));
    }
}
