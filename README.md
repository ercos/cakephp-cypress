# CakePHP Cypress

This package allows you to run Cypress tests on a CakePHP project with factories and fixtures and is inspired on the [Laravel Cypress](https://github.com/laracasts/cypress) package.

## Requirements

- CakePHP 4.0+
- Cypress 10.0+
- PHP 8.0+
- https://github.com/josegonzalez/php-dotenv
- https://github.com/vierge-noire/cakephp-fixture-factories is highly recommended to take full advantage of this package

## API Setup

1. Install the package :`composer require ercos/cakephp-cypress`
2. Load the plugin in the `src/Application.php` file : `bin/cake plugin load Ercos/CakephpCypress`
3. Extract your database structure (all tables + phinxlog structure + data) as an SQL file.
4. Add your SQL file to your project, for example in `/dump/my-database.sql`.
5. Copy `config/.env` to `config/.env.cypress`. **IMPORTANT :** make sure to set your testing database on both DB_DATABASE (default) and DB_TEST_DATABASE (test) env variables.
6. Add the path to the sql file in the `config/.env.cypress` file, as `export SQL_DUMP_FOLDER="/dump/my-database.sql"`

7. Add the following code to your `.htaccess` file :

```
RewriteEngine On
SetEnvIf x-cypress-header true cypress
...
```
This will allow the plugin to detect if the request is coming from Cypress and automatically switch to the testing database (according to `.env.cypress`)

8. In your `config/bootstrap.php` file, add the logic to load the `.env.cypress` file if the request is coming from Cypress. You should find the code part to update right after the imports in the `config/bootstrap.php` file.

```php
if (!env('APP_NAME')) {
    $dotenv = '';
    if (env('cypress') && file_exists(CONFIG . '.env.cypress')) {
        $dotenv = new Loader([CONFIG . '.env.cypress']);
    } elseif (file_exists(CONFIG . '.env')) {
        $dotenv = new Loader([CONFIG . '.env']);
    }
    if ($dotenv instanceof Loader) {
        $dotenv->parse()
        ->toEnv();
    }
}
```
9. In your `src/Application.php`, add the following code to the `bootstrap()` function :

```php
if (env('cypress')) {
    require TESTS . 'bootstrap.php';
}
```
10. In your `tests/bootstrap.php` file, do not import the bootstrap.php file when Cypress is running as shown :

```php
if (!env('cypress', false)) {
    require dirname(__DIR__) . '/config/bootstrap.php';
}
```
11. In your `tests/bootstrap.php` file, Add the following code to fix the usage of sessions.<br/> Fixate sessionid early on, as php7.2+ does not allow the sessionid to be set after stdout has been written to.
```php
if (!env('cypress', false)) {
    session_id('cli');
}
```

## Front-end setup

1. `npm install cypress --save-dev` 
2. Copy the **content** of the `front-end` folder to the root of the front-end project.
3. Update the Cypress configuration (depending on your Cypress version, either use the `cypress.json` or `cypress.config.ts` file as an example)
4. Add your fixtures to enable to database refresh command in `tests/cypress/support/commands.js`

```
body: {
    fixtures: [
        "app.Users",
        ...
    ]
},
```

## Available commands

- `refreshDatabase` : to call in the Cypress before hook. The command drops all the tables, executes the SQL file to create a fresh database, and finally runs the migrations. You can update the file from time to time to speed up the test setup time.
- `create` : to create a record in the database using factories. For example, `cy.create('Users', { username: 'John' })` will create a user with the username "John".
- `login` : to login a user. You need to complete the `login` function in `tests/cypress/support/commands.js` to make it work with your front-end project.

## Usage

1. Create tests in `tests/cypress/integration` folder
2. Run `npx cypress open`

