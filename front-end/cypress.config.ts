import { defineConfig } from 'cypress';

export default defineConfig({
  chromeWebSecurity: false,
  retries: 1,
  defaultCommandTimeout: 7500,
  watchForFileChanges: true,
  screenshotsFolder: 'tests/cypress/screenshots',
  screenshotOnRunFailure: false,
  video: false,
  fixturesFolder: 'tests/cypress/fixtures',
  e2e: {
    // We've imported your old cypress plugins here.
    // You may want to clean this up later by importing these.
    setupNodeEvents(on, config) {
      return require('./tests/cypress/plugins/index.js')(on, config);
    },
    baseUrl: 'http://localhost:4200',
    specPattern: 'tests/cypress/integration/**/*.cy.{js,jsx,ts,tsx}',
    supportFile: 'tests/cypress/support/index.js',
    viewportWidth: 1280,
    viewportHeight: 720,
  },
});
