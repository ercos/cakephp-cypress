/// <reference types="cypress" />

declare namespace Cypress {
    interface Chainable<Subject> {
        /**
         * Logs in the user.
         *
         * @example
         * cy.login()
         */
        login(): Chainable<any>;

        /**
         * Create and persist a new record using a factory.
         *
         * @example
         * cy.create({ factory: '\\App\\User' })
         * cy.create({
         *      factory: '\\App\\User',
         *      state: { email: 'test@test.com' }
         * })
         * cy.create({
         *      factory: '\\App\\User',
         *      with_associations: true // Will call the withAssociations method on the factory
         * })
         * cy.create({
         *      factory: '\\App\\User',
         *      state: { email: 'test@test.com' },
         *      with: [{ model: 'Roles', state: {level: 5}}]
         * })
         * cy.create({
         *      factory: '\\App\\User',
         *      state: { email: 'test@test.com' },
         *      custom_methods: [{ name: 'withRoles', parameters: [5]}]
         * })
         */
        create(props: {
            factory: string;
            state?: object;
            with_associations?: boolean;
            with?: { model: string; state: object }[]
            custom_methods?: { name: string; parameters: any[] }[]
        }): Chainable<any>;

        /**
         * Refresh the database state
         *
         * @example
         * cy.refreshDatabase()
         */
        refreshDatabase(): Chainable<any>;
    }
}
